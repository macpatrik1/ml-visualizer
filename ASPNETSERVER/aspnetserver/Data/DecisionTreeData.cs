﻿using System.Diagnostics;

namespace aspnetserver.Data
{
    public class DecisionTreeData
    {
    public async Task<string> StartPythonScript(string arguments)
    {
        ProcessStartInfo startInfo = new()
        {
            FileName = "C:\\Users\\Patrik\\AppData\\Local\\Programs\\Python\\Python311\\python.exe",
            Arguments = "C:\\Users\\Patrik\\Desktop\\project\\PythonScripts\\DecisionTree\\" + arguments,
            RedirectStandardOutput = true,
            UseShellExecute = false,
            CreateNoWindow = true
        };

        using Process process = new()
        {
            StartInfo = startInfo
        };

        process.Start();

        string output = await process.StandardOutput.ReadToEndAsync();

        // Wait for the process to exit asynchronously
        await process.WaitForExitAsync();

        return output;

    }

    }

}
