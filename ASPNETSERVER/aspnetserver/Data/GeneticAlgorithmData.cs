﻿namespace aspnetserver.Data
{
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;

    public class GeneticAlgorithmData
    {
        public GeneticAlgorithmData()
        {
            Y = new List<List<float>>();
        }
        public List<float>? X { get; set; }
        public List<float>? trueY { get; set; }
        public List<List<float>>? Y { get; set; }

        public void ReadFileContents(string filePath)
        {
            List<string> lines = new();
            int counter = 0;

            using StreamReader reader = new(filePath);
            string? line = reader.ReadLine();

            if (line is not null)
            {
                X = line.Replace("[", "").Replace("]", "").Split(",").Select(float.Parse).ToList();
            }

            line = reader.ReadLine();
            if (line is not null) {
                trueY = line.Replace("[", "").Replace("]", "").Split(",").Select(float.Parse).ToList();
            };

            while ((line = reader.ReadLine()) is not null)
            {
                if(counter % 10  == 0)
                {
                    Y.Add(line.Split(",").Select(float.Parse).ToList());
                };
                counter++;
            }
        }

        public async Task<string> StartPythonScript(string arguments)
        {
            ProcessStartInfo startInfo = new()
            {
                FileName = "C:\\Users\\Patrik\\AppData\\Local\\Programs\\Python\\Python311\\python.exe",
                Arguments = "C:\\Users\\Patrik\\Desktop\\project\\PythonScripts\\GeneticAlgorithm\\" + arguments,
                RedirectStandardOutput = true,
                UseShellExecute = false,
                CreateNoWindow = true
            };

            using Process process = new()
            {
                StartInfo = startInfo
            };

            // Start the process asynchronously
            process.Start();

            // Read the standard output asynchronously
            string output = await process.StandardOutput.ReadToEndAsync();

            // Wait for the process to exit asynchronously
            await process.WaitForExitAsync();

            return output;
        }
    }

}
