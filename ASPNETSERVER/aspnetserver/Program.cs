using aspnetserver.Data;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using System.Text.Json;
using System.Text.Json.Nodes;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddCors(options =>
{
    options.AddPolicy("CORSPolicy", builder =>
    {
        builder
            .AllowAnyMethod()
            .AllowAnyHeader()
            .WithOrigins("http://localhost:3000");
    });
});

builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseCors("CORSPolicy");
app.UseHttpsRedirection();


app.MapGet("/genetic-algorithm", async (string function, string neuralNet, string popSize, string eliteSize, string probability, string deviation, string iterations) =>
{

    GeneticAlgorithmData geneticAlgorithm = new();
    string arguments = "solution.py " + "--train " + function + " --nn " + neuralNet + " --popsize " + popSize + " --elitism " + eliteSize + " --p " + probability + " --K " + deviation + " --iter " + iterations;
    await geneticAlgorithm.StartPythonScript(arguments);
    geneticAlgorithm.ReadFileContents("C:\\Users\\Patrik\\Desktop\\project\\PythonScripts\\GeneticAlgorithm\\genetic_algorithm_result.txt");
    return JsonSerializer.Serialize(geneticAlgorithm);
});


app.MapPost("/genetic-algorithm/upload", (string fileName, [FromBody]FileContent fileContent) =>
 {
    try
     {
         StreamWriter sw = new("C:\\Users\\Patrik\\Desktop\\project\\PythonScripts\\GeneticAlgorithm\\" + fileName);
         sw.WriteLine(fileContent.Result);
         sw.Close();
     }
     catch(Exception e) { 
        Console.WriteLine("Exception: " + e.Message);
     }
});

app.MapGet("/decision-tree/train", async (string filePath) =>
{
    DecisionTreeData decisionTreeData = new();
    string arguments = "solution.py " + filePath;
    string result = await decisionTreeData.StartPythonScript(arguments);
    return result;
});

app.MapGet("/backpropagation", async (string coordinates) =>
{
    BackpropagationData backpropagationData = new();
    string arguments = "novo_predict.py " + coordinates;
    string result = await backpropagationData.StartPythonScript(arguments);
    return result;
});

app.Run();
