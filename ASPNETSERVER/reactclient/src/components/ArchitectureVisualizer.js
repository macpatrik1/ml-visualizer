import React from 'react'

const CIRCLE_RADIUS = 10

const ArchitectureVisualizer = ({architecture, width, height}) => {
  
  const drawCircles = (nn) => {
    const hiddenLayer = nn.split("s").slice(0,-1).map((value) => {
      return parseInt(value)
    })

    const widthBetweenCirclesX = (width - ((hiddenLayer.length + 2) * 2 * CIRCLE_RADIUS)) / (hiddenLayer.length + 1)
    hiddenLayer.unshift(1)
    hiddenLayer.push(1)
    const circles = []
    let fromLeft = 0

    hiddenLayer.forEach((layer) => {
      const widthBetweenCirclesY = (height - (layer * 2 * CIRCLE_RADIUS)) / (layer + 1)
      let fromTop = widthBetweenCirclesY + CIRCLE_RADIUS
      fromLeft += CIRCLE_RADIUS

      for(let j = 0; j < layer ; j++) {  
        circles.push(<circle cx={fromLeft} cy={fromTop} r="9" stroke="green" strokeWidth="1" fill="green" />)
        fromTop += 2 * CIRCLE_RADIUS + widthBetweenCirclesY
      } 

      fromLeft += CIRCLE_RADIUS + widthBetweenCirclesX
    })
    return circles
  }

  const drawLines = (nn) => {
    const hiddenLayer = nn.split("s").slice(0,-1).map((value) => {
      return parseInt(value)
    })

    const widthBetweenLinesX = (width - ((hiddenLayer.length + 2) * 2 * CIRCLE_RADIUS)) / (hiddenLayer.length + 1)

    hiddenLayer.unshift(1)
    hiddenLayer.push(1)
    const lines = []

    let fromLeftFirst = 0
    for(let i = 0; i < hiddenLayer.length; i++) {
      if(i === hiddenLayer.length - 1)
        break

      fromLeftFirst += CIRCLE_RADIUS * 2
      const widthBetweenLinesLeftFirst = (height - (hiddenLayer[i] * 2 * CIRCLE_RADIUS)) / (hiddenLayer[i] + 1)
      let fromTopFirst = widthBetweenLinesLeftFirst + CIRCLE_RADIUS
      const fromLeftSecond = fromLeftFirst + widthBetweenLinesX
      const widthBetweenLinesLeftSecond = (height - (hiddenLayer[i + 1] * 2 * CIRCLE_RADIUS)) / (hiddenLayer[i + 1] + 1)
      for(let j = 0; j < hiddenLayer[i]; j++) {
        let fromTopSecond = widthBetweenLinesLeftSecond + CIRCLE_RADIUS
        for(let k = 0; k < hiddenLayer[i + 1]; k++) {
          lines.push(<line x1={fromLeftFirst} y1={fromTopFirst} x2={fromLeftSecond} y2={fromTopSecond} stroke="black"  strokeWidth="2"/>)
          fromTopSecond += widthBetweenLinesLeftSecond + 2 * CIRCLE_RADIUS
        }
        fromTopFirst += 2 * CIRCLE_RADIUS + widthBetweenLinesLeftFirst
      }
      fromLeftFirst += widthBetweenLinesX
    }
    return lines
  }

  return (
    <>
    <div style={{width: width + 'px', height: height + 'px', margin: 'auto'}}>
      <svg viewBox={`0 0 ${width} ${height}`}>
        {drawCircles(architecture)}
        {drawLines(architecture)}
      </svg>
    </div>
    </>
  )
}

export default ArchitectureVisualizer