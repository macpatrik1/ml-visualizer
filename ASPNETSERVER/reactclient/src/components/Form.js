import React, {useState} from 'react'
import Button from 'react-bootstrap/Button'
import Form from 'react-bootstrap/Form'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import { BsFillTrashFill } from 'react-icons/bs';

const MyForm = ({runGeneticAlgorithm, stopGeneticAlgorithm, handleParentArchitectureChange }) => {
    const [neuralNet, setNeuralNet] = useState("5s")
    const [popSize, setPopSize] = useState(10)
    const [eliteSize, setEliteSize] = useState(1)
    const [probability, setProbability] = useState(0.1)
    const [deviation, setDeviation] = useState(0.1)
    const [iterations, setIterations] = useState(100)
    const [mathFunction, setMathFunction] = useState("sine_train.txt")
    const [importedFile, setImportedFile] = useState("")

    const handleArchitectureChange = (e) => {
        const regex = /^(([1-9]|1[0-9]|20)s)+$/;
        setNeuralNet(e.target.value)
        
        if(regex.test(e.target.value)) {
            handleParentArchitectureChange(e.target.value)
        }

        if(e.target.value === "")
        handleParentArchitectureChange("5s")
    }

    const loadTxtFile = () => {
        let input = document.createElement("input");
        input.type = "file";
        input.accept = "." + "txt";

        input.onchange = (_) => {
            let files = Array.from(input.files);
            const reader = new FileReader();
            reader.readAsText(files[0]);
            const fileName = files[0].name
            reader.onloadend = function () {
                const url = `https://localhost:7029/genetic-algorithm/upload?fileName=${fileName}`
                fetch(url, {
                    method: 'POST',
                    headers: {
                        "Content-Type": "application/json"
                    },
                    body: JSON.stringify({result: reader.result})
                })
                .then(response => {
                    if(response.ok) {
                        setImportedFile(fileName)
                    }
                })
                .catch(error => console.log(error))
            }
        }

        input.click()
    }
    
    return (
      <>
          <Form>
               <Form.Group className="mb-3" controlId="formBasicNN">
                   <Form.Label>Architecture type</Form.Label>
                   <Form.Control value={neuralNet} type='text' placeholder='5s' onChange={(e) => handleArchitectureChange(e)}/>
               </Form.Group>
               <Form.Group className="mb-3" controlId="formPopSize">
                   <Form.Label>Population size</Form.Label>
                   <Form.Control value={popSize} type="number" placeholder="10" step="1" onChange={(e) => setPopSize(e.target.value)}/>
               </Form.Group>
               <Form.Group className="mb-3" controlId="formEliteSize">
                   <Form.Label>Elite size</Form.Label>
                   <Form.Control value={eliteSize} type="number" placeholder="1" step="1" onChange={(e) => setEliteSize(e.target.value)}/>
               </Form.Group>
               <Form.Group className="mb-3" controlId="formProbability">
                   <Form.Label>Probability of mutation</Form.Label>
                   <Form.Control value={probability} type="number" placeholder="0.1" step="0.1" onChange={(e) => setProbability(e.target.value)}/>
               </Form.Group>
               <Form.Group className="mb-3" controlId="formDeviation">
                   <Form.Label>Standard deviation of Gaussian noise</Form.Label>
                   <Form.Control value={deviation} type="number" placeholder="0.1" step="0.1" onChange={(e) => setDeviation(e.target.value)}/>
               </Form.Group>
               <Form.Group className="mb-3" controlId="formIterations">
                   <Form.Label>Number of iterations</Form.Label>
                   <Form.Select aria-label="Default select example" onChange={(e) => setIterations(e.target.value)}>
                       <option value="100">100</option>
                       <option value="1000">1000</option>
                       <option value="2000">2000</option>
                       <option value="4000">4000</option>
                       <option value="6000">6000</option>
                       <option value="8000">8000</option>
                       <option value="10000">10000</option>
                   </Form.Select>
               </Form.Group>
               <Form.Group className="mb-3" controlId="formMathFunction">
                   <Form.Label>Select math function</Form.Label>
                   <Form.Select aria-label="Default select example" disabled={importedFile !== ""} onChange={(e) => setMathFunction(e.target.value)}>
                       <option value="sine_train.txt">Sine wave</option>
                       <option value="cos_train.txt">Cosine function</option>
                       <option value="square_train.txt">Square function</option>
                   </Form.Select>
               </Form.Group>
               {importedFile !== "" &&
               <Row style={{'marginBottom' : '1rem'}}>
                    <Col>
                        <Form.Group>
                            <Form.Label>Imported file</Form.Label>
                            <Form.Control value={importedFile} type='text' readOnly/>
                        </Form.Group>
                    </Col>
                    <Col>
                        <BsFillTrashFill style={{'color' : 'red'}} onClick={() => setImportedFile("")}/>
                    </Col>
               </Row>
               }
               <div className='button-container'>
                <Button onClick={() => runGeneticAlgorithm(neuralNet, popSize, eliteSize, probability, deviation, iterations, mathFunction, importedFile)}>
                    Submit
                </Button>
                <Button className='stop-button' onClick={() => stopGeneticAlgorithm()}>
                    Stop
                </Button>
                {importedFile === "" && 
                <Button className='import-button' onClick={() => loadTxtFile()}>
                    Import
                </Button>
                }
               </div>
          </Form>
      </>
    )
}

export default MyForm