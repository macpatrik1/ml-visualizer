import '../style/navbar.css'
import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import NavDropdown from 'react-bootstrap/NavDropdown';
import React from "react";
import { Outlet } from 'react-router-dom';

export default function NavBar() {
    return (
      <>
        <Navbar expand="lg">
          <Container>
            <Navbar.Brand href="/">Algorithms</Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
              <Nav className="me-auto">
                <Nav.Link href="/">Home</Nav.Link>
                <NavDropdown title="Algorithm select" id="basic-nav-dropdown">
                  <NavDropdown.Item href="genetic-algorithm">Genetic Algorithm</NavDropdown.Item>
                  <NavDropdown.Item href="decision-tree">Decision tree</NavDropdown.Item>
                  <NavDropdown.Item href="backpropagation">Backpropagation</NavDropdown.Item>
                </NavDropdown>
                <Nav.Link href="about">About</Nav.Link>
              </Nav>
            </Navbar.Collapse>
          </Container>
        </Navbar>
        <Outlet />
      </>
      );
}