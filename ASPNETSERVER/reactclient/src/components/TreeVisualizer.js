import React, { useState, useEffect } from 'react'
import getTextWidth from '../utils/svg'
const CIRCLE_RADIUS = 10

const TreeVisualizer = ({data, depth, width, height}) => {

  const widthBetweenNamesY = (height - depth * (2 * CIRCLE_RADIUS)) / (depth + 1)
  const [lines, setLines] = useState([])
  const [text, setText] = useState([])
  const [names, setNames] = useState([])
  const [isLoading, setIsLoading] = useState(true);
   
  const drawNames = (data, depth, widthBetweenNamesX, minusX, Y, wX) => {
    //draw circle
    const textWidth = getTextWidth(data.name.toUpperCase() || data.value, "Arial", "14px")
    const text = <text x={widthBetweenNamesX - minusX - (textWidth / 2)} y={Y + 15} fontFamily='Arial' fontSize={14} fontWeight="bold" fill={data.name ? "red" : "green"}>{data.name.toUpperCase() || data.value}</text>

    setNames(prevArray => [...prevArray, text])
    
    if(data.children.length > 0) {
      const widthX = wX / data.children.length
      widthBetweenNamesX -= widthX * (data.children.length - 1)
      minusX = widthX / 2
      for(let i = 0; i < data.children.length; i++) {
        drawNames(data.children[i], depth, widthBetweenNamesX, minusX , Y + widthBetweenNamesY + 2 * CIRCLE_RADIUS, widthX)
        widthBetweenNamesX += widthX
      }
    }
  }

  const drawLines = (data, depth, parentX, parentY, widthBetweenNamesX, minusX, Y, wX, first, nextValue) => {
    const newX = widthBetweenNamesX - minusX
    if(data.children.length > 0) {
      const newParentX = widthBetweenNamesX - minusX
      const newParentY = Y + 20 
      const widthX = wX / data.children.length
      widthBetweenNamesX -= widthX * (data.children.length - 1)
      minusX = widthX / 2
      for(let i = 0; i < data.children.length; i++) {
        drawLines(data.children[i], depth, newParentX, newParentY, widthBetweenNamesX, minusX, Y + widthBetweenNamesY + 2 * CIRCLE_RADIUS, widthX, false, Array.from(data.next_values)[i])
        widthBetweenNamesX += widthX
      }
    }

    if(!first) {
      const percent = 0.1
      const widthY = Math.abs(Y - parentY)
      const widthX = Math.abs(newX - parentX)
      const middleCoordinateX = (newX + parentX) / 2
      const middleCoordinateY = (Y + parentY) / 2

      const line1 = <line x1={parentX} y1={parentY} x2={parentX > middleCoordinateX ? middleCoordinateX + (percent * widthX) : middleCoordinateX - (percent * widthX)} y2={middleCoordinateY - (percent * widthY)} stroke="black"/> 
      const line2 = <line x1={parentX > middleCoordinateX ? middleCoordinateX - (percent * widthX) : middleCoordinateX + (percent * widthX)} y1={middleCoordinateY + (percent * widthY)} x2={newX} y2={Y} stroke="black"/>
      const textWidth = getTextWidth(nextValue, "Arial", "14px") 
      const text = <text x={middleCoordinateX - (textWidth / 2)} y={middleCoordinateY + (percent * widthY) / 2} fontFamily='Arial' fontSize={14}>{nextValue}</text>
      setLines(prevArray => [...prevArray, line1, line2])  
      setText(prevArray => [...prevArray, text])  
    }
  }


  useEffect(() => {
    if(Object.keys(data).length !== 0) {
      drawNames(data, depth, width, width / 2, widthBetweenNamesY, width)
      drawLines(data, depth, null, null, width, width / 2, widthBetweenNamesY, width, true)
      setIsLoading(false);
    }
  }, [data])

  if (isLoading) {
    return <div>Loading...</div>;
  }
  return (
    <>
    <div style={{width: width + 'px', height: height + 'px', margin: 'auto'}}>
      <svg viewBox={`0 0 ${width} ${height}`}>
        {names}
        {lines}
        {text}
      </svg>
    </div>
  </>
  )
}

export default TreeVisualizer