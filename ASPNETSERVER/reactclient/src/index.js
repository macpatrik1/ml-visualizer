import 'bootstrap/dist/css/bootstrap.css';
import './style/index.css'
import React from "react";
import ReactDOM from "react-dom/client";
import { createBrowserRouter, RouterProvider } from "react-router-dom";
import Home from './pages/Home';
import NavBar from './components/NavBar';
import GeneticAlgorithm from './pages/GeneticAgorithm';
import DecisionTree from './pages/DecisionTree'
import Backpropagation from './pages/Backpropagation';

const router = createBrowserRouter([
    {
        element: <NavBar/>,
        children: [
            {
                path: "/",
                element: <Home/>
            },
            {
                path: "/genetic-algorithm",
                element: <GeneticAlgorithm />
            },
            {
                path: "/decision-tree",
                element: <DecisionTree />
            },
            {
                path: "/backpropagation",
                element: <Backpropagation />
            }
        ]
    }
])
ReactDOM.createRoot(document.getElementById("root")).render(<RouterProvider router={router} />);