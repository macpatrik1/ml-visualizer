import React, { useState, useRef } from 'react';
import ReactECharts from 'echarts-for-react';

const Backpropagation = () => {
    const [drawing, setDrawing] = useState(false);
    const [coordinate, setCoordinate] = useState([]);
    const [line, setLine] = useState([]);
    const [classification, setClassification] = useState({})
    const [graphData, setGraphData] = useState([])
    const svgRef = useRef(null); 

    const option = {
        xAxis: {
          type: 'category',
          data: ['alfa', 'delta', 'beta', 'gama']
        },
        yAxis: {
          type: 'value'
        },
        series: [
          {
            data: graphData,
            type: 'bar'
          }
        ]
    };

    const selectEquallySpacedObjects = (numObjectsToSelect) => {
        const selectedObjects = {}

        for(const key in classification) {
            selectedObjects[key] = []
            classification[key].forEach(line => {
                const totalObjects = line.length;
                if (totalObjects <= numObjectsToSelect) {
                    selectedObjects[key].push(line.props.x1 + ',' + line.props.y1);
                }
                const stepSize = Math.ceil(totalObjects / numObjectsToSelect);
                const obj = [];
                
                for (let i = 0; i < numObjectsToSelect; i++) {
                    const index = i * stepSize;
                    obj.push(line[index].props.x1 + ',' + line[index].props.y1);
                }
                
                selectedObjects[key].push(obj)
            })
        }

        return selectedObjects
    };

    const lineEqualySpaced = (numObjectsToSelect) => {
        let s = ""
        const totalObjects = line.length;
        if(totalObjects <= numObjectsToSelect) {
            line.forEach(value => {
                s += value.props.x1 + "," + value.props.y1 + ","
            })

            return s
        }
        
        const stepSize = Math.ceil(totalObjects / numObjectsToSelect);
        for(let i = 0; i < numObjectsToSelect; i++) {
            let index = i * stepSize;
            if(index >= totalObjects)
                index = totalObjects - 1
            s += line[index].props.x1 + "," + line[index].props.y1 + ","
        }
        
        return s
    }

    const startDrawing = () => {
        const index = String(Object.keys(classification).length)
        setClassification((prevState) => {
            return { ...prevState, [index]: [] };
        });
    }

    const stopDrawing = () => {
        const selectedObjects = selectEquallySpacedObjects(20)
        for(const key in selectedObjects) {
            let s = ""
            selectedObjects[key].forEach(line => {
                s = line.toString()
            })
        }
    }

    const onMouseDown = (e) => {
        setDrawing(true);
        const dim = svgRef.current.getBoundingClientRect();
        var x = e.clientX - dim.left;
        var y = e.clientY - dim.top;
        setCoordinate([x, y]);
    }

    const onMouseMove = (e) => {
        if (!drawing)
            return;

            const dim = svgRef.current.getBoundingClientRect();
        var x = e.clientX - dim.left;
        var y = e.clientY - dim.top;
        const newLine = <line x1={coordinate[0]} y1={coordinate[1]} x2={x} y2={y} stroke="black" />
        setCoordinate([x,y])
        setLine(prevArray => [...prevArray, newLine]);
    }

    const onMouseUp = () => {
        setGraphData([])
        // const index = String(Object.keys(classification).length) - 1

        // setClassification((prevState) => {
        //     return { ...prevState, [index]:  [...prevState[index], line]};
        // });
        const line = lineEqualySpaced(20)
        const url = `https://localhost:7029/backpropagation?coordinates=${line.substring(0, line.length - 1)}`
        fetch(url, {
            method: 'GET'
        })
        .then(response => response.text())
        .then(data => {
            data.split(",").forEach(value => {
                setGraphData(prevArray => [...prevArray, value])
            })
        })
        setLine([]);
        setCoordinate([]);
        setDrawing(false);
    }

    return (
        <>
            <button onClick={() => startDrawing()}>Start drawing</button>
            <button onClick={() => stopDrawing()}>StopDrawing</button>
            <div style={{ width: '600px', height: '600px', margin: 'auto', border: '1px solid black' }}>
                <svg ref={svgRef} viewBox="0 0 600 600" onMouseDown={onMouseDown} onMouseMove={onMouseMove} onMouseUp={onMouseUp}>
                    {line}
                </svg>
            </div>

            <ReactECharts option={option} />
        </>
    )
}

export default Backpropagation;
