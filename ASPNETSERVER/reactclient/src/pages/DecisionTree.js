import '../style/form.css'
import React, { useState, useEffect } from 'react';
import TreeVisualizer from '../components/TreeVisualizer';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Form  from 'react-bootstrap/Form';
import { Button } from 'react-bootstrap';
import { makeTree } from '../utils/treeMaker';

const DecisionTree = () => {
  const [depth, setDepth] = useState(0);
  const [treeData, setTreeData] = useState({});
  const [isLoading, setIsLoading] = useState(false);
  const [fileName, setFileName] = useState("volleyball.csv")

  const generateDecisionTree = () => {
    setIsLoading(true)
    setTimeout(() => {
      const url = `https://localhost:7029/decision-tree/train?filePath=${fileName}`;
      fetch(url, {
        method: 'GET',
      })
        .then((response) => response.text())
        .then((data) => {
          const dataTree = makeTree(data);
          setDepth(dataTree.maxDepth);
          setTreeData(dataTree.treeData)
          setIsLoading(false);
        });
    }, 0);
  }


  return (
    <>
      <Container>
        <Row>
          <Col>
            <Form>
              <Form.Group className="mb-3" controlId="formFile">
                <Form.Label>Select File</Form.Label>
                <Form.Select aria-label="Default select example" onChange={(e) => setFileName(e.target.value)}>
                  <option value="volleyball.csv">volleyball</option>
                  <option value="nezaboravno_ljeto.csv">nezaboravno_ljeto</option>
                  <option value="programski_jezik.csv">programski_jezik</option>
                  <option value="ljudi.csv">ljudi</option>
                </Form.Select>
              </Form.Group>
              <div className='button-container'>
                <Button onClick={() => generateDecisionTree()}>
                  Submit
                </Button>
              </div>
            </Form>
          </Col>
          <Col>
            {Object.keys(treeData).length > 0 && !isLoading  && <TreeVisualizer data={treeData} depth={depth + 1} width={1200} height={600}/>}
          </Col>
        </Row>
      </Container>
    </>
  );
};

export default DecisionTree;
