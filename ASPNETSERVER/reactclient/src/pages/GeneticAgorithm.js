import '../style/form.css'
import React, { useState } from "react";
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import ReactECharts from 'echarts-for-react';
import MyForm from "../components/Form";
import ArchitectureVisualizer from "../components/ArchitectureVisualizer";

export default function GeneticAlgorithm() {
    const [ySeriesData, setYSeriesData] = useState([])
    const [yTrueSeriesData, setyTrueSeriesData] = useState([])
    const [xSeriesData, setXSeriesData] = useState([])
    const [minY, setMinY] = useState(0)
    const [maxY, setMaxY] = useState(10)
    const [architecture, setArchitecture] = useState("5s")
    const [timeoutIDs, setTimeoutIDs] = useState([])

    const options = {
        borderRadius: 10,
        backgroundColor: '#F3FFFA',
        grid: { top: 13, right: 8, bottom: 24, left: 50},
        xAxis: {
          type: 'category',
          data: xSeriesData,
          axisLine: {
            lineStyle: {
              color: "black"
            }
        }
        },
        yAxis: {
          type: 'value',
          min: minY,
          max: maxY,
          axisLine: {
              lineStyle: {
                color: "black"
              }
          }
        },
        series: [
          {
            data: ySeriesData,
            type: 'line',
            smooth: true,
            lineStyle: {
                color: 'black',
                width: 6,
            }
          },
          {
            data: yTrueSeriesData,
            type: 'line',
            smooth: true,
            lineStyle: {
                color: '#008B4F',
                width: 3,
            }  
        }
        ],
        tooltip: {
          trigger: 'axis',
        },
      };


    const runGeneticAlgorithm = (neuralNet, popSize, eliteSize, probability, deviation, iterations, mathFunction, importedFile) => {
        const url = `https://localhost:7029/genetic-algorithm?neuralNet=${neuralNet}&popSize=${popSize}&eliteSize=${eliteSize}&probability=${probability}&deviation=${deviation}&iterations=${iterations}&function=${importedFile === "" ? mathFunction : importedFile}`
        console.log(url)
        fetch(url, {
          method: 'GET',
        })
        .then(response => response.json())
        .then(data => {
            setMinY((Math.min(...data.Y[data.Y.length - 1]) - 2.5).toFixed(2))
            setMaxY((Math.max(...data.Y[data.Y.length - 1]) + 2.5).toFixed(2))
            setXSeriesData(data.X)
            setyTrueSeriesData(data.trueY)
            data.Y.forEach((arrayValue, index) => {
                timeoutIDs.push(setTimeout(() => {
                    setYSeriesData(arrayValue)
                }, index * 50))
                setTimeoutIDs(timeoutIDs)
            })
        })
        .catch(error => console.log(error))
    }

    const stopGeneticAlgorithm = () => {
      console.log(timeoutIDs)
      timeoutIDs.forEach(timeoutID => clearTimeout(timeoutID))
      setTimeoutIDs([])
    }
    const handleArchitectureChange = (architecture) => {
        setArchitecture(architecture)
    }
    
    return (
        <>
            <Container className="mt-3">
                <Row>
                    <Col>
                        <MyForm runGeneticAlgorithm={runGeneticAlgorithm} stopGeneticAlgorithm={stopGeneticAlgorithm} handleParentArchitectureChange={handleArchitectureChange}/>
                    </Col>
                    <Col>
                        <ArchitectureVisualizer architecture={architecture} width={600} height={400}/>
                        <ReactECharts option={options} />
                    </Col>
                </Row>
            </Container>
        </>
    );
}