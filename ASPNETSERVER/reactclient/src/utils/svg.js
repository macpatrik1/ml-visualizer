export default function getTextWidth(text, font, fontSize) {
    const svg = document.createElementNS("http://www.w3.org/2000/svg", "svg");
    svg.setAttribute("visibility", "hidden");

    const textElement = document.createElementNS("http://www.w3.org/2000/svg", "text");
    textElement.setAttribute("font-family", font);
    textElement.setAttribute("font-size", fontSize);
    textElement.textContent = text;
    svg.appendChild(textElement);

    document.body.appendChild(svg);
    const bbox = textElement.getBBox();
    document.body.removeChild(svg);
    return bbox.width;
  }