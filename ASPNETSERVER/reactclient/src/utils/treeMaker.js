
export const makeTree = (s) => {
    const treeData = {}
    s = s.substring(0, s.length - 1)
    const lines = s.split('\n');
    let maxDepth = 0;

    lines.forEach((line) => {
      const nodes = line.split(' ');
      nodes.forEach((node, index) => {
        const pattern = /:(.*?)=/;
        const pattern2 = /=(.*)$/;
        const numbers = node.match(/^\d+/);
        if (numbers && maxDepth < parseInt(numbers[0])) {
          maxDepth = parseInt(numbers[0]);
        }

        const match = node.match(pattern);
        let parentNode = treeData;
        if (index > 0) {
          parentNode = findNode(nodes[index - 1].match(pattern)[1], treeData);
        }
        if (Object.keys(parentNode).length === 0) {
          parentNode['next_values'] = new Set([node.match(pattern2)[1]]);
          parentNode['value'] = '';
          parentNode['name'] = match[1];
          parentNode['children'] = [];
        }
        if (match != null) {
          const currentNode = findNode(match[1], treeData);
          if (currentNode === null || currentNode === undefined) {
            const newNode = {
              next_values: new Set([node.match(pattern2)[1]]),
              value: '',
              name: match[1],
              children: [],
            };
            parentNode.children.push(newNode);
          } else {
            currentNode.next_values.add(node.match(pattern2)[1]);
          }
        } else {
          const newNode = {
            next_values: null,
            value: node,
            name: '',
            children: [],
          };
          parentNode.children.push(newNode);
        }
      });
    });
    return {treeData, maxDepth};
  };
  
  const findNode = (name, node) => {
    if (node == undefined || Object.keys(node).length === 0) {
      return null;
    }
    if (node.name === name) {
      return node;
    } else {
      for (let i = 0; i < node.children.length; i++) {
        const result = findNode(name, node.children[i]);
        if (result !== undefined) {
          return result;
        }
      }
    }
  };