import numpy as np

class Dense:
    def __init__(self, input_dim, ouput_dim) -> None:
        self.w = np.random.rand(input_dim, ouput_dim) - 0.5
        self.b = np.random.rand(input_dim, 1) - 0.5
        self.input = None
        self.x = None
        
    def forward(self, x):
        result = np.dot(self.w, x) + self.b
        self.x = x
        return result

    def backward(self, grad_output, learning_rate):
        dW = 1 / 21 * np.dot(grad_output, self.input.T)
        dB = 1 / 21 * np.sum(grad_output)
        self.w -= learning_rate * dW
        self.b -= learning_rate * dB

        new_grad = np.dot(self.w.T, grad_output) * (self.x > 0)

        return new_grad
    
class NeuralNet: 
    def __init__(self) -> None:
        self.params = []
    
    def add_layer(self, layer):
        self.params.append(layer)

    def forward(self, x: np.ndarray):
        y = x
        for i, layer in enumerate(self.params):
            layer.input = y
            y = layer.forward(y)
            if i<len(self.params)-1:
                y = np.maximum(y, 0)

        return self.softmax(y)
    
    def backpropagation(self, x, y_true, learning_rate):
        y_pred = self.forward(x)
        loss = self.cross_entropy_loss(y_pred, y_true)

        grad_output = (y_pred - y_true)

        for layer in reversed(self.params):
            grad_output = layer.backward(grad_output, learning_rate)

        return loss, y_pred
    
    def cross_entropy_loss(self, predicted, target):
        num_samples = target.shape[0]
        loss = -np.sum(target * np.log(predicted + 1e-9)) / num_samples
        return loss
    
    @staticmethod
    def softmax(z):
        return np.exp(z) / sum(np.exp(z))