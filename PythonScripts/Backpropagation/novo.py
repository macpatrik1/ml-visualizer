import numpy as np
import os
import pickle



if __name__ == "__main__" :
    def load_csv(filename: str):
        script_dir = os.path.dirname(os.path.abspath(__file__))
        filepath = os.path.join(script_dir, filename)
        try:
            xy = np.loadtxt(filepath, delimiter=',', skiprows=1)
        except Exception as e:
            print(f"Error occurred while loading file: {e}", flush=True)
        return xy
    
    data = load_csv("proba.csv")
    m, n = data.shape
    np.random.shuffle(data) # shuffle before splitting into dev and training sets
    data_train = data.T
    Y_train = data_train[0]
    X_train = data_train[1:n]
    X_train = X_train / 600
    _,m_train = X_train.shape

    # data_test = load_csv("proba_test.csv")
    # m, n = data.shape
    # np.random.shuffle(data_test) # shuffle before splitting into dev and training sets
    # data_dev = data_test.T
    # Y_dev = data_dev[0]
    # X_dev = data_dev[1:n]
    # X_dev = X_dev / 600


    def init_params():
        W1 = np.random.rand(10, 40) - 0.5
        b1 = np.random.rand(10, 1) - 0.5
        W2 = np.random.rand(4, 10) - 0.5
        b2 = np.random.rand(4, 1) - 0.5
        return W1, b1, W2, b2

    def ReLU(Z):
        return np.maximum(Z, 0)

    def softmax(Z):
        A = np.exp(Z) / sum(np.exp(Z))
        return A

    def forward_prop(W1, b1, W2, b2, X):
        Z1 = W1.dot(X) + b1
        A1 = ReLU(Z1)
        Z2 = W2.dot(A1) + b2
        A2 = softmax(Z2)
        return Z1, A1, Z2, A2

    def ReLU_deriv(Z):
        return Z > 0

    def one_hot(Y):
        one_hot_Y = np.zeros((Y.size, Y.astype(int).max() + 1))
        one_hot_Y[np.arange(Y.size), Y.astype(int)] = 1
        one_hot_Y = one_hot_Y.T
        return one_hot_Y

    def backward_prop(Z1, A1, Z2, A2, W1, W2, X, Y):
        one_hot_Y = one_hot(Y)
        dZ2 = A2 - one_hot_Y
        dW2 = 1 / m * dZ2.dot(A1.T)
        db2 = 1 / m * np.sum(dZ2)
        dZ1 = W2.T.dot(dZ2) * ReLU_deriv(Z1)
        dW1 = 1 / m * dZ1.dot(X.T)
        db1 = 1 / m * np.sum(dZ1)
        return dW1, db1, dW2, db2

    def update_params(W1, b1, W2, b2, dW1, db1, dW2, db2, alpha):
        W1 = W1 - alpha * dW1
        b1 = b1 - alpha * db1    
        W2 = W2 - alpha * dW2  
        b2 = b2 - alpha * db2    
        return W1, b1, W2, b2
    
    def get_predictions(A2):
        return np.argmax(A2, 0)

    def get_accuracy(predictions, Y):
        print(predictions, Y)
        return np.sum(predictions == Y) / Y.size

    def gradient_descent(X, Y, alpha, iterations):
        W1, b1, W2, b2 = init_params()
        for i in range(iterations):
            Z1, A1, Z2, A2 = forward_prop(W1, b1, W2, b2, X)
            dW1, db1, dW2, db2 = backward_prop(Z1, A1, Z2, A2, W1, W2, X, Y)
            W1, b1, W2, b2 = update_params(W1, b1, W2, b2, dW1, db1, dW2, db2, alpha)
            if i % 10 == 0:
                print("Iteration: ", i)
                predictions = get_predictions(A2)
                print(get_accuracy(predictions, Y))
        return W1, b1, W2, b2
    
    def make_predictions(X, W1, b1, W2, b2):
        _, _, _, A2 = forward_prop(W1, b1, W2, b2, X)
        predictions = get_predictions(A2)
        return predictions

    # def test_prediction(index, W1, b1, W2, b2):
    #     prediction = make_predictions(X_dev[:, index, None], W1, b1, W2, b2)
    #     label = Y_dev[index]
    #     print("Prediction: ", prediction)
    #     print("Label: ", label)
    #     print("==============================")

    def write_to_file(filename: str, content: np.ndarray):
        script_dir = os.path.dirname(os.path.abspath(__file__))
        filepath = os.path.join(script_dir, filename)
        try:
            with open(filepath, "wb") as file:
                pickle.dump(content, file)
        except Exception as e:
            print(f"Error occurred while loading file: {e}", flush=True)

W1, b1, W2, b2 = gradient_descent(X_train, Y_train, 0.10, 300)


write_to_file("w1.pkl", W1)
write_to_file("b1.pkl", b1)
write_to_file("w2.pkl", W2)
write_to_file("b2.pkl", b2)

    
# with open("data.pkl", "rb") as file:
#     loaded_array = pickle.load(file)

# print(loaded_array)
# for i in range(8):
#     test_prediction(i, W1, b1, W2, b2)
