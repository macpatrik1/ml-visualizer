import os
import pickle
import numpy as np
import sys
import re

def load_matrix(filename: str):
    script_dir = os.path.dirname(os.path.abspath(__file__))
    filepath = os.path.join(script_dir, filename)
    try:
        with open(filepath, "rb") as file:
            matrix = pickle.load(file)
    except Exception as e:
        print(f"Error occurred while loading file: {e}", flush=True)
    return matrix

def forward_prop(W1, b1, W2, b2, X):
    Z1 = W1.dot(X) + b1
    A1 = ReLU(Z1)
    Z2 = W2.dot(A1) + b2
    A2 = softmax(Z2)
    return A2

def ReLU(Z):
    return np.maximum(Z, 0)

def softmax(Z):
    A = np.exp(Z) / sum(np.exp(Z))
    return A

if __name__ == "__main__":
    W1 = load_matrix("w1.pkl")
    b1 = load_matrix("b1.pkl")
    W2 = load_matrix("w2.pkl")
    b2 = load_matrix("b2.pkl")

    train_data_string = sys.argv[1]
    train_data = np.array([float(x) for x in train_data_string.split(",")]).reshape(-1,1) / 600
    
    result = ','.join(str(x) for x in forward_prop(W1,b1,W2,b2,train_data))
    pattern = r"[\[\]]"
    result_string = re.sub(pattern, "", result)

    print(result_string)


