from neuralnet import NeuralNet, Dense
import numpy as np
import os

if __name__ == "__main__":
    neural_net = NeuralNet()
    
    neural_net.add_layer(Dense(10, 40))
    neural_net.add_layer(Dense(4, 10))

    def load_csv(filename: str):
        script_dir = os.path.dirname(os.path.abspath(__file__))
        filepath = os.path.join(script_dir, filename)
        try:
            xy = np.loadtxt(filepath, delimiter=',', skiprows=1)
        except Exception as e:
            print(f"Error occurred while loading file: {e}", flush=True)
        return xy

    def one_hot_encode(arr, num_classes):
        one_hot_encoded = np.zeros((len(arr), num_classes))
        for i, val in enumerate(arr):
            one_hot_encoded[i, int(val)] = 1
        return one_hot_encoded
    
    data = load_csv("proba.csv")
    m, n = data.shape
    data_train = data.T
    Y_train = data_train[0]
    X_train = data_train[1:n] / 600

    data_test = load_csv("proba_test.csv")
    data_dev = data_test.T
    Y_dev = data_dev[0]
    X_dev = data_dev[1:n] / 600


    true_output = one_hot_encode(Y_train, 4).T
   
    for i in range(1000):
        backerr, y_pred = neural_net.backpropagation(X_train, true_output, 0.1)

        if(backerr < 0.001):
            break

        if i % 1 == 0:
            print(f"Epoch {i}, Loss: {backerr:.4f}")

    output = neural_net.forward(X_dev)
    print("Final output predictions:", output)