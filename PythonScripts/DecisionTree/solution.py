from math import log2
import csv
import sys
import os
prediction = ""
pred = "no"


class Node:
    def __init__(self, leaf=False, value=None, name=None, most_common_value=None) -> None:
        self.leaf = leaf
        self.children = {}
        self.value = value
        self.name = name
        self.most_common_value = most_common_value

    def __str__(self) -> str:
        return self.name + self.children + self.leaf


class ID3():
    def __init__(self) -> None:
        None

    def fit(self, D, D_parent, X, y, max_depth):
        return self.tree(D, D_parent, X, y, max_depth)

    def predict(self, prediction_tree, predict_state):
        global prediction
        if prediction_tree.leaf:
            print(prediction_tree.value, end=" ")
            prediction = prediction_tree.value
        else:
            value = predict_state[prediction_tree.name]
            if prediction_tree.children.get(value) is not None:
                node = prediction_tree.children[value]
                self.predict(node, predict_state)
            else:
                prediction = prediction_tree.most_common_value
                print(prediction, end=" ")

    def check(self, D_parent):
        if(len(D_parent) == 0):
            return True
        else:
            return False

    def calculate_entropy(self, D):
        y = {}
        for line in D:
            last_key = list(line)[-1]
            y[line[last_key]] = y.get(line[last_key], 0) + 1

        entropy = 0
        for key in y:
            entropy += -y[key]/len(D)*log2(y[key]/len(D))

        return entropy

    def argmax(self, D_parent):
        if self.check(D_parent):
            return pred
        last_key = list(D_parent[0].keys())[-1]
        y_array = [x[last_key] for x in D_parent]

        count = {}
        for value in y_array:
            if value in count:
                count[value] += 1
            else:
                count[value] = 1
        max_v = max(count.values())
        common = [k for k, v in count.items() if v == max_v]
        common.sort()
        return common[0]

    def IG(self, Dx, x, key):
        entropy_of_Dx = self.calculate_entropy(Dx)
        for value in x:
            sub_Dx = [d for d in Dx if d.get(key) == value]
            entropy_of_value = self.calculate_entropy(sub_Dx)
            entropy_of_Dx -= len(sub_Dx) / len(Dx) * entropy_of_value
        return entropy_of_Dx

    def get_largest_IG_value(self, list1_numbers, list2_string):
        largest_number = max(list1_numbers)
        max_index = list1_numbers.index(largest_number)
        names = [list2_string[index] for index, value in enumerate(
            list1_numbers) if value == largest_number]
        if(len(names) > 1):
            return min(names)
        else:
            return list2_string[max_index]

    def tree(self, D, D_parent, X, y, max_depth):

        if(len(D) == 0 or max_depth == 0):
            v = self.argmax(D_parent)
            return Node(leaf=True, value=v)

        v = self.argmax(D)
        if(len(X) == 0 or self.calculate_entropy(D) == 0 or max_depth == 0):
            return Node(leaf=True, value=v)

        x_list = []
        for x in X:
            x_list.append(self.IG(D_parent, X[x], x))

        x_keys = list(X.keys())
        x = self.get_largest_IG_value(x_list, x_keys)
        node = Node(name=x)
        node.most_common_value = v
        for v in X.get(x):
            new_D = [y for y in D if y.get(x) == v]
            new_X = X.copy()
            new_X.pop(x)
            child_node = self.tree(new_D, new_D, new_X, y, max_depth - 1)
            node.children[v] = child_node
        return node


def print_tree(node, depth, path):
    if node.leaf:
        s = path + node.value
        s = s.replace("\n", "")
        print(s)
        return
    else:
        path += str(depth)+":"+node.name+"="
        for child in node.children:
            last_index = path.rfind("=")
            path = path[0:last_index+1]
            path += child + " "
            print_tree(node.children[child], depth + 1, path)


if __name__ == "__main__":

    # path_to_tree = "volleyball.csv"
    # path_to_test = "volleyball_test.csv"
    file_name = sys.argv[1]
    script_dir = os.path.dirname(os.path.abspath(__file__))
    file_path = os.path.join(script_dir, file_name)
    # path_to_test = sys.argv[2]

    if(len(sys.argv) > 3):
        max_depth = int(sys.argv[3])
    else:
        max_depth = sys.maxsize

    model = ID3()

    X = {}
    D = []
    y = set()
    with open(file_path, encoding="utf-8") as csvfile:
        reader = csv.DictReader(csvfile)
        first_row = list(reader.fieldnames)
        for value in first_row[:-1]:
            X[value] = set()
        for row in reader:
            row_string = ""
            dict = {}
            for value in first_row:
                if X.get(value) is not None:
                    X[value].add(row[value])

                row_string += row[value]+","
                dict[value] = row[value]
            D.append(dict)
            y.add(row_string.split(",")[-2])

    # confusion matrix
    confusion_matrix = {}
    sorted_y = list(y)
    sorted_y = sorted(sorted_y)
    for yy in sorted_y:
        confusion_matrix[yy] = {}
        for yyy in sorted_y:
            confusion_matrix[yy][yyy] = 0

    # tree
    tree = model.fit(D, D, X, y, max_depth)
    print_tree(tree, 1, "")

    X_to_test = {}
    counter = 0
    lines = 0

    # with open(path_to_test, encoding="UTF-8") as csvfile:
    #     print("[PREDICTIONS]: ", end="")
    #     reader = csv.DictReader(csvfile)
    #     first_row = list(reader.fieldnames)
    #     for value in first_row:
    #         X_to_test[value] = ""
    #     for row in reader:
    #         lines += 1
    #         for value in first_row:
    #             X_to_test[value] = (row[value])

    #         model.predict(tree, X_to_test)

    #         # populate confusion matrix
    #         confusion_matrix[prediction][X_to_test[list(
    #             X_to_test.keys())[-1]]] += 1
    #         if(X_to_test[list(X_to_test.keys())[-1]] == prediction):
    #             counter += 1
    #         for value in first_row:
    #             del X_to_test[value]

    #         for value in first_row:
    #             X_to_test[value] = ""

    # # print ACCURACY
    # print()

    # print("[ACCURACY]: " + "{:.5f}".format(round(counter / lines, 5)))

    # print("[CONFUSION_MATRIX]:")
    # # print confusion matrix
    # rows = list(confusion_matrix.keys())
    # cols = list(confusion_matrix[rows[0]].keys())
    # matrix = [[confusion_matrix[row][col] for col in cols] for row in rows]
    # matrix = list(map(list, zip(*matrix)))
    # for row in matrix:
    #     print(*row)
