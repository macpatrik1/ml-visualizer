from neuralnet import NeuralNet, Linear
import numpy as np
from util import mean_squared_error, write_to_file
import random



def crossover(nn1: NeuralNet, nn2: NeuralNet) -> NeuralNet:
    child = NeuralNet()
    for l1, l2 in zip(nn1.params, nn2.params):
        res_layer = Linear()
        res_layer.w = (l1.w + l2.w)/2
        res_layer.b = (l1.b + l2.b)/2
        child.params.append(res_layer)
    return child


def mutation(nn: NeuralNet, p: float, k: float) -> None:

    for layer in nn.params:
        shape_w = np.shape(layer.w)
        shape_b = np.shape(layer.b)
        mutation_mask_w = np.random.random_sample(size=shape_w) < p
        mutation_mask_b = np.random.random_sample(size=shape_b) < p

        w_noise = np.where(mutation_mask_w, np.random.normal(loc=0, scale=k, size=shape_w), 0)
        b_noise = np.where(mutation_mask_b, np.random.normal(loc=0, scale=k, size=shape_b), 0)

        layer.w += w_noise
        layer.b += b_noise


def selection(pop_range: list, weights: list):
    a = random.choices(pop_range, weights)[0]
    w_cache = weights[a]
    weights[a] = 0.0
    b = random.choices(pop_range, weights)[0]
    weights[a] = w_cache
    return a,b


def __evaluate_population(population: list, pop_errors: dict, errors: list, fitness: list, x: np.ndarray, y: np.ndarray):
    # funkcija ocekuje da su pop_errors, errors i fitness prazni
    y_pred = []
    for p in population:
        y_pred = p.forward(x)
        pop_errors[p] = mean_squared_error(y, y_pred)

    population.sort(key=pop_errors.get)
    for count, p in enumerate(population):
        if count == 0:
            write_to_file(filename="genetic_algorithm_result.txt", content=y_pred)
        err = pop_errors[p]
        errors.append(err)
        fitness.append(1/err)



def train(population: list, x_train: np.ndarray, y_train: np.ndarray, elitism: int, p: float, k: float, n_iter: int, update: int = 2000):
    write_to_file(filename="genetic_algorithm_result.txt", content=x_train)
    write_to_file(filename="genetic_algorithm_result.txt", content=y_train)
    parents = population.copy()
    pop_size = len(population)
    children = []
    population_errors = {}
    errors = []
    fitness = []

    pop_range = [i for i in range(pop_size)] #za potrebe selekcije

    __evaluate_population(parents, population_errors, errors, fitness, x_train, y_train)

    for i in range(1, n_iter+1):
        
        # elitizam - u sljedecu generaciju se prenosi najboljih jedan ili više jedinki
        children.extend(parents[:elitism])

        # selekcija, krizanje, mutacija
        while len(children) < pop_size:
            r1_idx, r2_idx = selection(pop_range, weights=fitness)
            child = crossover(nn1=parents[r1_idx], nn2=parents[r2_idx])
            mutation(child, p, k)
            children.append(child)
        
        parents.clear()
        parents, children = children, parents
        population_errors.clear()
        errors.clear()
        fitness.clear()
        __evaluate_population(parents, population_errors, errors, fitness, x_train, y_train)
        if i%update == 0:
            fmt_err = "{:.6f}".format(errors[0])
            print(f"[Train error @{i}]: {fmt_err}")
    
    return parents, errors
        
    
    