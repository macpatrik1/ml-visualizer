import numpy as np
from util import sigm


class Linear:
    def __init__(self, in_features: int = None, out_features: int = None, initialize: bool = False) -> None:
        self.w = None
        self.b = None
        if initialize:
            shape_w = (out_features, in_features) if out_features>1 else in_features
            shape_b = out_features if out_features>1 else None
            self.w = np.random.normal(loc=0, scale=0.01, size=shape_w)
            self.b = np.random.normal(loc=0, scale=0.01, size=shape_b)
    
    def forward(self, x):
        return np.dot(x, self.w.T) + self.b




class NeuralNet:
    def __init__(self) -> None:
        self.params = []


    def forward(self, x: np.ndarray):
        y = x
        for i, layer in enumerate(self.params):
            y = layer.forward(y)
            if i<len(self.params)-1:
                y = sigm(y)

        return y


    @staticmethod
    def from_config(cfg: str, input_features: int):
        splitted = cfg.split(sep="s")
        neural_net = NeuralNet()
        n_in = input_features
        n_out = None
        for v in splitted:
            if len(v)==0:
                break
            n_out = int(v)
            neural_net.params.append(Linear(in_features=n_in, out_features=n_out, initialize=True))
            n_in = n_out
        
        # zadnji sloj
        neural_net.params.append(Linear(in_features=n_in, out_features=1, initialize=True))
        return neural_net
        


