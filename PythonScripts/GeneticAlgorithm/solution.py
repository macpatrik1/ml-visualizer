import numpy as np
import argparse
import util
import geneticalgorithm as ga
from neuralnet import NeuralNet


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--train", type=str, required=True)
    # parser.add_argument("--test", type=str, required=False)
    parser.add_argument("--nn", type=str, required=True)
    parser.add_argument("--popsize", type=int, required=True)
    parser.add_argument("--elitism", type=int, required=True)
    parser.add_argument("--p", type=float, required=True)
    parser.add_argument("--K", type=float, required=True)
    parser.add_argument("--iter", type=int, required=True)
    args = parser.parse_args()
    
    x_train, y_train = util.load_csv(args.train)

    input_features = x_train.shape[1]
    # x_test, y_test = util.load_csv(args.test)
    #clear file content
    util.clear_file(filename="genetic_algorithm_result.txt")
    
    population = []
    for i in range(args.popsize):
        nn = NeuralNet.from_config(cfg=args.nn, input_features=input_features)
        population.append(nn)

    
    population_after, errors = ga.train(population, x_train, y_train, args.elitism, args.p, args.K, args.iter)

    # best = population_after[0]
    # y_pred = best.forward(x_test)
    # test_err = util.mean_squared_error(y_test, y_pred)
    # test_err_fmt = "{:.6f}".format(test_err)
    # print(f"[Test error]: {test_err_fmt}")
    
