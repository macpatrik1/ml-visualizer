import numpy as np
import os


def load_csv(filename: str):
    script_dir = os.path.dirname(os.path.abspath(__file__))
    filepath = os.path.join(script_dir, filename)
    try:
        xy = np.loadtxt(filepath, delimiter=',', skiprows=1)
    except Exception as e:
        print(f"Error occurred while loading file: {e}", flush=True)
    return xy[:,:-1], xy[:,-1]



def sigm(x):
    e_x = np.exp(-np.abs(x))
    return np.where(x>0, 1./(1+e_x), e_x/(1+e_x))



def mean_squared_error(y_true: np.ndarray, y_pred: np.ndarray):
    diff = y_true - y_pred
    return np.dot(diff,diff)/y_true.size
        
def write_to_file(filename: str, content: np.ndarray):
    script_dir = os.path.dirname(os.path.abspath(__file__))
    filepath = os.path.join(script_dir, filename)
    try:
        f = open(filepath, "a")  
        for value in content[:-1]:
            f.write(str(value)[:7] + ",")
        f.write(str(content[-1])[:7] + "\n")
        f.close()
    except Exception as e:
        print(f"Error occurred while loading file: {e}", flush=True)

def clear_file(filename: str):
    script_dir = os.path.dirname(os.path.abspath(__file__))
    filepath = os.path.join(script_dir, filename)
    try:
        open(filepath, "w").close()
    except Exception as e:
        print(f"Error occurred while loading file: {e}", flush=True)